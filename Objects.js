//function keys(obj) {
// Retrieve all the names of the object's properties.
// Return the keys as strings in an array.
// Based on http://underscorejs.org/#keys
//}
//const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

function keys(obj) {

    if (typeof obj !== 'object') {
        return "Please provide  an object"
    }

    let newArr = [];

    for (key in obj) {
        newArr.push(key);
    }

    return newArr;
}

//function values(obj) {
// Return all of the values of the object's own properties.
// Ignore functions
// http://underscorejs.org/#values
//}

function values(obj) {

    if (typeof obj !== 'object') {
        return "Please provide  an object"
    }

    let newArr = [];

    for (key in obj) {
        if (obj[key].constructor === Function) {
            continue;
        } 

        else newArr.push(obj[key]);
            
    }

    return newArr;
}

//function mapObject(obj, cb) {
// Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
// http://underscorejs.org/#mapObject
//}

//var myObject = { 'a': 1, 'b': 2, 'c': 3 };

function mapObject(obj, cb) {

    if (typeof obj !== 'object') {
        return "Please provide  an object"

    }

    for (let key in obj) {

        if (obj[key]) {
            let newObj = cb(obj[key]);
            obj[key] = newObj;
        }
    }

    return obj;
}


//function pairs(obj) {
// Convert an object into a list of [key, value] pairs.
// http://underscorejs.org/#pairs
//}

//function pairs(obj) {
// Convert an object into a list of [key, value] pairs.
// http://underscorejs.org/#pairs


function pairs(obj) {

    if (typeof obj !== 'object') {
        return "Please provide  an object"
    }

    let newArr = [];

    for (key in obj) {
        if (obj[key].constructor === Function) {
            continue;

        } else newArr.push([key, obj[key]]);
            
    }

    return newArr;
}

//function invert(obj) {
// Returns a copy of the object where the keys have become the values and the values the keys.
// Assume that all of the object's values will be unique and string serializable.
// http://underscorejs.org/#invert
//}

function invert(obj) {

    if (typeof obj !== 'object') {
        return "Please provide  an object"

    }

    var inverseObj = {};

    for (var key in obj) {
        inverseObj[obj[key]] = key;
    }

    return inverseObj;
}

//function defaults(obj, defaultProps) {
// Fill in undefined properties that match properties on the `defaultProps` parameter object.
// Return `obj`.
// http://underscorejs.org/#defaults
//}



function defaults(obj, defaultProps) {

    if (typeof obj !== 'object' && typeof defaultProps !== 'object') {
        return "Please provide  an object"
    }

    let newObj = { ...obj }

    for (let key in defaultProps) {
        if (!(key in newObj)) {
            newObj[key] = defaultProps[key];
        }
    }

    return newObj
}


module.exports = { keys, values, mapObject, pairs, invert, defaults };