// Problem1 forEach

function each(array, callback) {
    if (!Array.isArray(array) && callback !== 'function') {
        return "Please Provide an array, callback function";
    }

    for (let i = 0; i < array.length; i++) {
        let element = array[i];
        callback(element, i, array);
    }
}

// function for map method

function map(array, callback) {
    if (!Array.isArray(array) && callback !== 'function') {
        return "Please Provide an array, callback function";
    }

    let mapped = [];

    for (let i = 0; i < array.length; i++) {
        let result = callback(array[i])
        mapped.push(result);
    }
    return mapped;
}


// function for reduce method

function reduce(array, callback, startingValue) {
    if (!Array.isArray(array) && callback !== 'function') {
        return "Please Provide an array, callback function";
    }

    let value;
    if (startingValue) {
        value = startingValue;
    }

    else {
        value = array[0];
    }

    if (startingValue) {
        for (let i = 0; i < array.length; i++) {
            value = callback(value, array[i]);
        }

    } else {
        for (let i = 1; i < array.length; i++) {
            value = callback(value, array[i]);
        }
    }
    return value;
}



// function for find method

function find(elements, cb) {
    if (!Array.isArray(array) && callback !== 'function') {
        return "Please Provide an array, callback function";
    }

    for (let key in elements) {
        if (cb(elements[key])==true) {
            return elements[key];
        }
    }

    return undefined;
}


// function for filter method

function filter(elements, cb) {
    if (!Array.isArray(array) && callback !== 'function'){
        return "Please Provide an array, callback function";
    }

    let filtered = [];

    for (let key in elements) {
        if (cb(elements[key])) {
            filtered.push(elements[key]);
        }
    }

    return filtered;
}

// function for flatten method


function flatten(array) {
    let flat = [];
    if (!Array.isArray(elements) ) {
        return "Please Provide an array, callback function and element to be found"
    }

    function rec(array) {   
    for (let i = 0; i < array.length; i++) {
        if (array[i].constructor === Array) {
           rec(array[i]);

        } else {
            flat.push(array[i]);
        }
    }
    
}

rec(array)
return flat;

}


module.exports = { each, map, reduce, find, filter, flatten };



