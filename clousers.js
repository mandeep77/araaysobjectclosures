// for counterFactory

function counterFactory() {

    var counter=0;

    let increment = function () {
        counter++;

        return counter;

    }

    let decrement = function () {
        counter--;

        return counter;
    }

    return { increment, decrement };
}




// for limitFunctionCallCount

let limitFunctionCallCount = function (func, n) {

    if(typeof func !== 'function' && typeof n !== 'number')
    {
        return "please provide function and a number";
    }

    let counter = 0;
    let temp;

    return function () {

        if (counter < n) {
            temp = func();
            counter++;
            return temp;
        }

        return null;
    }

};


// for cacheFunction

let cacheFunction = cb => {
    if(typeof cb != 'function'){
        return "please give a valid function";
    }
    let cache = {};
    return (...args) => {
        let index = JSON.stringify(args);
        if (!cache[index]) {
            return cache[index] = cb(...args);
        }
        return cache[index];

    }
}


module.exports = { counterFactory, limitFunctionCallCount, cacheFunction };